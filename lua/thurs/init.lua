require("thurs.packer")
require("thurs.remap")
require("thurs.set")

local augroup = vim.api.nvim_create_augroup
local ThursGroup = augroup('Thurs', {})

local autocmd = vim.api.nvim_create_autocmd
local yank_group = augroup('HighlightYank', {})

autocmd('TextYankPost', {
    group = yank_group,
    pattern = '*',
    callback = function()
        vim.highlight.on_yank({
            higroup = 'IncSearch',
            timeout = 500,
        })
    end,
})

autocmd({"BufWritePre"}, {
    group = ThursGroup,
    pattern = "*",
    command = [[%s/\s\+$//e]],
})

vim.g.netrw_browse_split = 0
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 25
vim.g.vim_isort_python_version = 'python3'
