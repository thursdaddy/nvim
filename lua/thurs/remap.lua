vim.g.mapleader = " "

vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Keep things centered
vim.keymap.set("n", "<C-h>", "<cmd>wincmd h<CR>zz")
vim.keymap.set("n", "<C-l>", "<cmd>wincmd l<CR>zz")
vim.keymap.set("n", "<C-j>", "<cmd>wincmd j<CR>zz")
vim.keymap.set("n", "<C-k>", "<cmd>wincmd k<CR>zz")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "}", "}zz")
vim.keymap.set("n", "{", "{zz")

vim.keymap.set("i", "<C-c>", "<Esc>")

vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")
vim.keymap.set("n", "J", "mzJ`z")

vim.keymap.set("n", "<C-i>", "<C-a>")
vim.keymap.set("n", "<C-d>", "<C-x>")
