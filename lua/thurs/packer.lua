-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'
  use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})
  use('nvim-treesitter/nvim-treesitter-context');
  use('theprimeagen/harpoon')
  use('mbbill/undotree')
  use('tpope/vim-fugitive')
  use('tpope/vim-surround')
  use({ 'junegunn/fzf', run = './install --bin', })
  use('stsewd/fzf-checkout.vim')
  use('fisadev/vim-isort')
  use('nvim-tree/nvim-web-devicons')
  use('folke/zen-mode.nvim')
  use('nvim-lua/plenary.nvim')
  use('nvim-pack/nvim-spectre')

  use {
      'lewis6991/gitsigns.nvim',
      config = function()
          require('gitsigns').setup()
      end
  }

  use({
    'nvim-lualine/lualine.nvim',
    requires = { 'nvim-tree/nvim-web-devicons', opt = true }
  })

  -- use('olimorris/onedarkpro.nvim')
  use { 'ellisonleao/gruvbox.nvim' }
  use { 'folke/tokyonight.nvim' }

  use {
	  'nvim-telescope/telescope.nvim', tag = '0.1.0',
	  requires = { {'nvim-lua/plenary.nvim'} }
  }
  use 'echasnovski/mini.comment'
  require('mini.comment').setup()

  -- A File Explorer For Neovim Written In Lua
  use({
      'kyazdani42/nvim-tree.lua',
      config = function()
          require('nvim-tree').setup()
      end,
      -- devicons in lua
      requires = { 'kyazdani42/nvim-web-devicons' },
  })

  use {
      'folke/trouble.nvim',
      requires = 'nvim-tree/nvim-web-devicons',
      config = function()
          require('trouble').setup {
              -- your configuration comes here
              -- or leave it empty to use the default settings
              -- refer to the configuration section below
          }
      end
  }

  use {
	  'VonHeikemen/lsp-zero.nvim',
	  requires = {
		  -- LSP Support
		  {'neovim/nvim-lspconfig'},
		  {'williamboman/mason.nvim'},
		  {'williamboman/mason-lspconfig.nvim'},

		  -- Autocompletion
		  {'hrsh7th/nvim-cmp'},
		  {'hrsh7th/cmp-buffer'},
		  {'hrsh7th/cmp-path'},
		  {'saadparwaiz1/cmp_luasnip'},
		  {'hrsh7th/cmp-nvim-lsp'},
		  {'hrsh7th/cmp-nvim-lua'},

		  -- Snippets
		  {'L3MON4D3/LuaSnip'},
		  {'rafamadriz/friendly-snippets'},
	  }
  }
end)
