-- Lua
vim.keymap.set("n", "<leader>e", "<cmd>NvimTreeToggle<cr>",
  {silent = true, noremap = true}
)
vim.keymap.set("n", "<leader>E", "<cmd>NvimTreeFocus<cr>",
  {silent = true, noremap = true}
)
