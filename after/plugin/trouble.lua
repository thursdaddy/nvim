-- Lua
vim.keymap.set("n", "<leader>ct", "<cmd>TroubleToggle<cr>",
  {silent = true, noremap = true}
)
